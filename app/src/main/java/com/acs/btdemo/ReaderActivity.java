package com.acs.btdemo;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ProgressDialog;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.acs.bluetooth.Acr1255uj1Reader;
import com.acs.bluetooth.Acr3901us1Reader;
import com.acs.bluetooth.BluetoothReader;
import com.acs.bluetooth.BluetoothReader.OnAuthenticationCompleteListener;
import com.acs.bluetooth.BluetoothReader.OnEnableNotificationCompleteListener;
import com.acs.bluetooth.BluetoothReader.OnResponseApduAvailableListener;
import com.acs.bluetooth.BluetoothReaderGattCallback;
import com.acs.bluetooth.BluetoothReaderGattCallback.OnConnectionStateChangeListener;
import com.acs.bluetooth.BluetoothReaderManager;
import com.acs.bluetooth.BluetoothReaderManager.OnReaderDetectionListener;

import javax.crypto.spec.SecretKeySpec;

public class ReaderActivity extends AppCompatActivity implements TxPowerDialogFragment.TxPowerDialogListener {

    //private MutableLiveData<Boolean > isLoading = new MutableLiveData<>();
    public static final String TAG = ReaderActivity.class.getSimpleName();
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    /* Default master key. */
    private static final String DEFAULT_3901_MASTER_KEY = "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF";
    /* Get 8 bytes random number APDU. */
    private static final String DEFAULT_3901_APDU_COMMAND = "80 84 00 00 08";

    /* Default master key. */
    private static final String DEFAULT_1255_MASTER_KEY = "ACR1255U-J1 Auth";

    /* Read 16 bytes from the binary block 0x04 (MIFARE 1K or 4K). */
    private static final String DEFAULT_1255_APDU_COMMAND = "FF CA 00 00 00";

    private static final byte[] AUTO_POLLING_START = {(byte) 0xE0, 0x00, 0x00,
            0x40, 0x01};

    private static final String DEFAULT_1255_Badge_COMMAND = "FF 82 00 00 06 FF FF FF FF FF FF";

    /* String keys for save/restore instance state. */
    private static final String AUTHENTICATION_KEY = "authenticatinKey";
    private static final String APDU_COMMAND = "apduCommand";
    private static final String RESPONSE_APDU = "responseApdu";

    /* Reader to be connected. */
    private String mDeviceName;
    private String mDeviceAddress;
    private int mConnectState = BluetoothReader.STATE_DISCONNECTED;

    private TextView mTxtAuthentication;
    private TextView mTxtResponseApdu;
    private TextView textView;

    private EditText mEditMasterKey;
    private EditText mEditApdu;

    private TextView cidTextView;
    private TextView badgeTextView;
    private TextView balanceTextView;
    private TextView hmacTextView;

    private String cidString;
    private String badgeString;

    /* Detected reader. */
    private BluetoothReader mBluetoothReader;
    /* ACS Bluetooth reader library. */
    private BluetoothReaderManager mBluetoothReaderManager;
    private BluetoothReaderGattCallback mGattCallback;

    private ProgressDialog mProgressDialog;

    /* Bluetooth GATT client. */
    private BluetoothGatt mBluetoothGatt;
    Timer timer = new Timer();

    /*
     * Listen to Bluetooth bond status change event. And turns on reader's
     * notifications once the card reader is bonded.
     */
    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothAdapter bluetoothAdapter;
            BluetoothManager bluetoothManager;

            final String action = intent.getAction();

            if (!(mBluetoothReader instanceof Acr3901us1Reader)) {
                /* Only ACR3901U-S1 require bonding. */
                return;
            }

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                Log.i(TAG, "ACTION_BOND_STATE_CHANGED");
                /* Get bond (pairing) state */
                if (mBluetoothReaderManager == null) {
                    Log.w(TAG, "Unable to initialize BluetoothReaderManager.");
                    return;
                }

                bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                if (bluetoothManager == null) {
                    Log.w(TAG, "Unable to initialize BluetoothManager.");
                    return;
                }

                bluetoothAdapter = bluetoothManager.getAdapter();
                if (bluetoothAdapter == null) {
                    Log.w(TAG, "Unable to initialize BluetoothAdapter.");
                    return;
                }

                final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(mDeviceAddress);

                if (device == null) {
                    return;
                }

                final int bondState = device.getBondState();

                /* Enable notification */
                if (bondState == BluetoothDevice.BOND_BONDED) {
                    if (mBluetoothReader != null) {
                        mBluetoothReader.enableNotification(true);
                    }
                }

                /* Progress Dialog */
                if (bondState == BluetoothDevice.BOND_BONDING) {
                    mProgressDialog = ProgressDialog.show(context,
                            "ACR3901U-S1", "Bonding...");
                } else {
                    if (mProgressDialog != null) {
                        mProgressDialog.dismiss();
                        mProgressDialog = null;
                    }
                }

            }
        }
    };

    /* Clear the Card reader's response and notification fields. */
    private void clearAllUi() {
        mTxtAuthentication.setText(R.string.noData);
        clearResponseUi();
    }

    /* Clear the Card reader's Response field. */
    private void clearResponseUi() {
        mTxtAuthentication.setText(R.string.noData);
        mTxtResponseApdu.setText(R.string.noData);
    }

    private void setListener(BluetoothReader reader) {
        /* Wait for authentication completed. */
        mBluetoothReader.setOnAuthenticationCompleteListener(new OnAuthenticationCompleteListener() {
            @Override
            public void onAuthenticationComplete(BluetoothReader bluetoothReader, final int errorCode) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
                            mTxtAuthentication.setText(getString(R.string.authentication_success));
                        } else {
                            mTxtAuthentication.setText(getString(R.string.authentication_failed));
                        }
                    }
                });
            }
        });

        mBluetoothReader.setOnEnableNotificationCompleteListener(new OnEnableNotificationCompleteListener() {
            @Override
            public void onEnableNotificationComplete(BluetoothReader bluetoothReader, final int result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result != BluetoothGatt.GATT_SUCCESS) {
                            /* Fail */
                            Toast.makeText(ReaderActivity.this, "The device is unable to set notification!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ReaderActivity.this, "The device is ready to use!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        });
    }

    /* Get the Response string. */
    private String getResponseString(byte[] response, int errorCode) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            if (response != null && response.length > 0) {
                return Utils.toHexString(response);
            }
            return "";
        }
        return getErrorString(errorCode);
    }

    private void btnAuthAction() {
        //isLoading.setValue(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mBluetoothReader == null) {
                    mTxtAuthentication.setText(R.string.card_reader_not_ready);
                    return;
                }
                /* Retrieve master key from edit box. */
                byte masterKey[] = Utils.getEditTextinHexBytes(mEditMasterKey);
                if (masterKey != null && masterKey.length > 0) {
                    /* Clear response field for the result of authentication. */
                    mTxtAuthentication.setText(R.string.noData);
                    /* Start authentication. */
                    if (!mBluetoothReader.authenticate(masterKey)) {
                        mTxtAuthentication.setText(R.string.card_reader_not_ready);
                    } else {
                        mTxtAuthentication.setText("Authenticating...");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btnStartPollingAction();
                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        btnTransmitCIDAction();
                                    }
                                }, 100, 100);
                            }
                        }, 500);
                    }
                } else {
                    mTxtAuthentication.setText("Character format error!");
                }
            }
        }, 500);
    }

    private void btnStartPollingAction() {
        if (mBluetoothReader == null) {
            Toast.makeText(ReaderActivity.this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_START)) {
            Toast.makeText(ReaderActivity.this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
        }
    }

    private void btnTransmitCIDAction() {
        if (mBluetoothReader == null) {
            mTxtResponseApdu.setText(R.string.card_reader_not_ready);
            return;
        }
        byte[] apduCommand = Utils.getEditTextinHexBytes(mEditApdu);
        if (apduCommand != null && apduCommand.length > 0) {
            /* Clear response field for result of APDU. */
            mTxtResponseApdu.setText(R.string.noData);
            /* Transmit APDU command. */
            if (!mBluetoothReader.transmitApdu(apduCommand)) {
                mTxtResponseApdu.setText(R.string.card_reader_not_ready);
            } else {
                mBluetoothReader.setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
                    @Override
                    public void onResponseApduAvailable(BluetoothReader bluetoothReader, final byte[] apdu, final int errorCode) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                byte[] apdu2 = Arrays.copyOfRange(apdu, 0, apdu.length - 2);
                                final long cid = toReversedDec(apdu2);
                                if (!getResponseString(apdu2, errorCode).isEmpty()) {
                                    mTxtResponseApdu.setText(cid + " " + getResponseString(apdu2, errorCode));

                                    cidString = cid + "";
                                    cidTextView.setText(cid + "");
                                    timer.cancel();

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            byte[] apduCommand1 = new byte[]{(byte) 0xFF, (byte) 0x82, (byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
                                            if (!mBluetoothReader.transmitApdu(apduCommand1)) {
                                                mTxtResponseApdu.setText(R.string.card_reader_not_ready);
                                            }
                                        }
                                    }, 500);


                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            byte[] apduCommand2 = new byte[]{(byte) 0xFF, (byte) 0x86, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x04, (byte) 0x60, (byte) 0x00};
                                            if (!mBluetoothReader.transmitApdu(apduCommand2)) {
                                                mTxtResponseApdu.setText(R.string.card_reader_not_ready);
                                            }
                                        }
                                    }, 1000);

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            byte[] apduCommand3 = new byte[]{(byte) 0xFF, (byte) 0xB0, (byte) 0x00, (byte) 0x04, (byte) 0x10};
                                            if (!mBluetoothReader.transmitApdu(apduCommand3)) {
                                                mTxtResponseApdu.setText(R.string.card_reader_not_ready);
                                            } else {
                                                mBluetoothReader.setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
                                                    @Override
                                                    public void onResponseApduAvailable(BluetoothReader bluetoothReader, final byte[] apdu, final int errorCode) {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                byte[] apdu2 = Arrays.copyOfRange(apdu, 2, 6);
                                                                Log.v("BADGE", apdu2 + "");
                                                                Log.v("BADGE2", apdu + "");
                                                                long badge = toReversedDec(apdu2);
                                                                badgeString = badge + "";
                                                                badgeTextView.setText(badge + "");



                                                                // navigate(cid + "", badge + "");
//                                                                Intent intent = new Intent(ReaderActivity.this, BalanceActivity.class);
//                                                                intent.putExtra("CID", cidString);
//                                                                intent.putExtra("BADGE", badgeString);
//                                                                startActivity(intent);
//                                                                finish();
                                                                //function which generate access token
                                                            }
                                                        });

                                                    }
                                                });
                                            }
                                        }
                                    }, 1500);
                                }
                            }
                        });
                    }
                });
            }
        } else {
            mTxtResponseApdu.setText("Character format error!");
        }
    }

    /* Start the process to enable the reader's notifications. */
    private void activateReader(BluetoothReader reader) {
        if (reader == null) {
            return;
        }

        if (reader instanceof Acr3901us1Reader) {
            /* Start pairing to the reader. */
            ((Acr3901us1Reader) mBluetoothReader).startBonding();
        } else if (mBluetoothReader instanceof Acr1255uj1Reader) {
            /* Enable notification. */
            mBluetoothReader.enableNotification(true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reader);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);


        //observe();
        /* Update UI. */
        findUiViews();
        updateUi(null);


        /* Initialize BluetoothReaderGattCallback. */
        mGattCallback = new BluetoothReaderGattCallback();

        /* Register BluetoothReaderGattCallback's listeners */
        mGattCallback.setOnConnectionStateChangeListener(new OnConnectionStateChangeListener() {
            @Override
            public void onConnectionStateChange(final BluetoothGatt gatt, final int state, final int newState) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (state != BluetoothGatt.GATT_SUCCESS) {
                            mConnectState = BluetoothReader.STATE_DISCONNECTED;
                            if (newState == BluetoothReader.STATE_CONNECTED) {
                                Toast.makeText(ReaderActivity.this, getString(R.string.connect_fail), Toast.LENGTH_SHORT).show();
                            } else if (newState == BluetoothReader.STATE_DISCONNECTED) {
                                Toast.makeText(ReaderActivity.this, getString(R.string.disconnect_fail), Toast.LENGTH_SHORT).show();
                            }
                            clearAllUi();
                            updateUi(null);
                            invalidateOptionsMenu();
                            return;
                        }
                        updateConnectionState(newState);
                        if (newState == BluetoothProfile.STATE_CONNECTED) {
                            /* Detect the connected reader. */
                            if (mBluetoothReaderManager != null) {
                                mBluetoothReaderManager.detectReader(gatt, mGattCallback);
                            }
                        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                            mBluetoothReader = null;
                            /*
                             * Release resources occupied by Bluetooth
                             * GATT client.
                             */
                            if (mBluetoothGatt != null) {
                                mBluetoothGatt.close();
                                mBluetoothGatt = null;
                            }
                        }
                    }
                });
            }
        });

        /* Initialize mBluetoothReaderManager. */
        mBluetoothReaderManager = new BluetoothReaderManager();

        /* Register BluetoothReaderManager's listeners */
        mBluetoothReaderManager.setOnReaderDetectionListener(new OnReaderDetectionListener() {
            @Override
            public void onReaderDetection(BluetoothReader reader) {
                updateUi(reader);
                if (reader instanceof Acr3901us1Reader) {
                    /* The connected reader is ACR3901U-S1 reader. */
                    Log.v(TAG, "On Acr3901us1Reader Detected.");
                } else if (reader instanceof Acr1255uj1Reader) {
                    /* The connected reader is ACR1255U-J1 reader. */
                    Log.v(TAG, "On Acr1255uj1Reader Detected.");
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ReaderActivity.this, "The device is not supported!", Toast.LENGTH_SHORT).show();
                            /* Disconnect Bluetooth reader */
                            Log.v(TAG, "Disconnect reader!!!");
                            disconnectReader();
                            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
                        }
                    });
                    return;
                }
                mBluetoothReader = reader;
                setListener(reader);
                activateReader(reader);
            }
        });

        /* Connect the reader. */
        connectReader();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportActionBar().setTitle(mDeviceName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

//    private void observe() {
//        final ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setTitle("Please Wait");
//        dialog.setMessage("Ay 7aga");
//        dialog.setCancelable(false);
//
//        isLoading.observe(this, new Observer<Boolean>() {
//            @Override
//            public void onChanged(@Nullable Boolean isLoading) {
//                if (isLoading){
//                    dialog.show();
//                }else {
//                    dialog.hide();
//                }
//            }
//        });
//    }

    private boolean connectReader() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            Log.w(TAG, "Unable to initialize BluetoothManager.");
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
            return false;
        }

        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter == null) {
            Log.w(TAG, "Unable to obtain a BluetoothAdapter.");
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
            return false;
        }

        /*
         * Connect Device.
         */
        /* Clear old GATT connection. */
        if (mBluetoothGatt != null) {
            Log.i(TAG, "Clear old GATT connection");
            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }

        /* Create a new connection. */
        final BluetoothDevice device = bluetoothAdapter.getRemoteDevice(mDeviceAddress);

        if (device == null) {
            Log.w(TAG, "Device not found. Unable to connect.");
            return false;
        }

        /* Connect to GATT server. */
        updateConnectionState(BluetoothReader.STATE_CONNECTING);
        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        return true;
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();

        final IntentFilter intentFilter = new IntentFilter();

        /* Start to monitor bond state change */
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver, intentFilter);

        /* Clear unused dialog. */
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();

        /* Stop to monitor bond state change */
        unregisterReceiver(mBroadcastReceiver);

        /* Disconnect Bluetooth reader */
        disconnectReader();
    }

    /* Disconnects an established connection. */
    private void disconnectReader() {
        if (mBluetoothGatt == null) {
            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
            return;
        }
        updateConnectionState(BluetoothReader.STATE_DISCONNECTING);
        mBluetoothGatt.disconnect();
    }

    /* Update the display of Connection status string. */
    private void updateConnectionState(final int connectState) {
        mConnectState = connectState;

        if (connectState == BluetoothReader.STATE_CONNECTING) {
            Toast.makeText(this, getString(R.string.connecting), Toast.LENGTH_SHORT).show();
        } else if (connectState == BluetoothReader.STATE_CONNECTED) {
            Toast.makeText(this, getString(R.string.connected), Toast.LENGTH_SHORT).show();
        } else if (connectState == BluetoothReader.STATE_DISCONNECTING) {
            Toast.makeText(this, getString(R.string.disconnecting), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, getString(R.string.disconnected), Toast.LENGTH_SHORT).show();
            clearAllUi();
            updateUi(null);
        }
        invalidateOptionsMenu();
    }

    /* Show and hide UI resources and set the default master key and commands. */


    private void updateUi(final BluetoothReader bluetoothReader) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (bluetoothReader instanceof Acr3901us1Reader) {
                    /* The connected reader is ACR3901U-S1 reader. */
                    if (mEditMasterKey.getText().length() == 0) {
                        mEditMasterKey.setText(DEFAULT_3901_MASTER_KEY);
                    }
                    if (mEditApdu.getText().length() == 0) {
                        mEditApdu.setText(DEFAULT_3901_APDU_COMMAND);
                    }

                    btnAuthAction();
                    mEditMasterKey.setEnabled(true);
                    mEditApdu.setEnabled(true);

                } else if (bluetoothReader instanceof Acr1255uj1Reader) {
                    /* The connected reader is ACR1255U-J1 reader. */
                    if (mEditMasterKey.getText().length() == 0) {
                        try {
                            mEditMasterKey.setText(Utils.toHexString(DEFAULT_1255_MASTER_KEY.getBytes("UTF-8")));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    if (mEditApdu.getText().length() == 0) {
                        mEditApdu.setText(DEFAULT_1255_APDU_COMMAND);
                    }


                    btnAuthAction();

                    mEditMasterKey.setEnabled(true);
                    mEditApdu.setEnabled(true);
                } else {
                    mEditApdu.setText(R.string.noData);
                    mEditMasterKey.setEnabled(false);
                    mEditApdu.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        /* Save the current state. */
        savedInstanceState.putString(AUTHENTICATION_KEY, mEditMasterKey.getText().toString());
        savedInstanceState.putString(APDU_COMMAND, mEditApdu.getText().toString());
        savedInstanceState.putString(RESPONSE_APDU, mTxtResponseApdu.getText().toString());

        /* Always call the superclass so it can save the view hierarchy state. */
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        /* Always call the superclass so it can restore the view hierarchy. */
        super.onRestoreInstanceState(savedInstanceState);
        /* Restore state members from saved instance. */
        mEditMasterKey.setText(savedInstanceState.getString(AUTHENTICATION_KEY));
        mEditApdu.setText(savedInstanceState.getString(APDU_COMMAND));
        mTxtResponseApdu.setText(savedInstanceState.getString(RESPONSE_APDU));
    }

    @Override
    public void onDialogItemClick(DialogFragment dialog, int which) {
        byte[] command = {(byte) 0xE0, 0x00, 0x00, 0x49, (byte) which};
        if (mBluetoothReader == null) {
            Toast.makeText(this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
            return;
        }
        if (!mBluetoothReader.transmitEscapeCommand(command)) {
            Toast.makeText(this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
        }
    }

    private long toReversedDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void findUiViews() {

        mTxtAuthentication = findViewById(R.id.textView_Authentication);
        mTxtResponseApdu = findViewById(R.id.textView_Response);

        mEditMasterKey = findViewById(R.id.editText_Master_Key);
        mEditApdu = findViewById(R.id.editText_ADPU);

        textView = findViewById(R.id.text);

        cidTextView = findViewById(R.id.cidTextView);
        badgeTextView = findViewById(R.id.badgeTextView);
        balanceTextView = findViewById(R.id.balanceTextView);
    }

    private String getErrorString(int errorCode) {
        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
            return "";
        } else if (errorCode == BluetoothReader.ERROR_INVALID_CHECKSUM) {
            return "The checksum is invalid.";
        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA_LENGTH) {
            return "The data length is invalid.";
        } else if (errorCode == BluetoothReader.ERROR_INVALID_COMMAND) {
            return "The command is invalid.";
        } else if (errorCode == BluetoothReader.ERROR_UNKNOWN_COMMAND_ID) {
            return "The command ID is unknown.";
        } else if (errorCode == BluetoothReader.ERROR_CARD_OPERATION) {
            return "The card operation failed.";
        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_REQUIRED) {
            return "Authentication is required.";
        } else if (errorCode == BluetoothReader.ERROR_LOW_BATTERY) {
            return "The battery is low.";
        } else if (errorCode == BluetoothReader.ERROR_CHARACTERISTIC_NOT_FOUND) {
            return "Error characteristic is not found.";
        } else if (errorCode == BluetoothReader.ERROR_WRITE_DATA) {
            return "Write command to reader is failed.";
        } else if (errorCode == BluetoothReader.ERROR_TIMEOUT) {
            return "Timeout.";
        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_FAILED) {
            return "Authentication is failed.";
        } else if (errorCode == BluetoothReader.ERROR_UNDEFINED) {
            return "Undefined error.";
        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA) {
            return "Received data error.";
        } else if (errorCode == BluetoothReader.ERROR_COMMAND_FAILED) {
            return "The command failed.";
        }
        return "Unknown error.";
    }

}














///*
// * Copyright (C) 2014-2018 Advanced Card Systems Ltd. All rights reserved.
// *
// * This software is the confidential and proprietary information of Advanced
// * Card Systems Ltd. ("Confidential Information"). You shall not disclose such
// * Confidential Information and shall use it only in accordance with the terms
// * of the license agreement you entered into with ACS.
// */
//package com.acs.btdemo;
//
//import java.io.UnsupportedEncodingException;
//import java.lang.reflect.Array;
//import java.util.Arrays;
//
//import android.app.ProgressDialog;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothGatt;
//import android.bluetooth.BluetoothManager;
//import android.bluetooth.BluetoothProfile;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.nfc.NfcAdapter;
//import android.nfc.Tag;
//import android.nfc.tech.MifareClassic;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.content.res.TypedArrayUtils;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.acs.bluetooth.Acr1255uj1Reader;
//import com.acs.bluetooth.Acr1255uj1Reader.OnBatteryLevelAvailableListener;
//import com.acs.bluetooth.Acr1255uj1Reader.OnBatteryLevelChangeListener;
//import com.acs.bluetooth.Acr3901us1Reader;
//import com.acs.bluetooth.Acr3901us1Reader.OnBatteryStatusAvailableListener;
//import com.acs.bluetooth.Acr3901us1Reader.OnBatteryStatusChangeListener;
//import com.acs.bluetooth.BluetoothReader;
//import com.acs.bluetooth.BluetoothReader.OnAtrAvailableListener;
//import com.acs.bluetooth.BluetoothReader.OnAuthenticationCompleteListener;
//import com.acs.bluetooth.BluetoothReader.OnCardPowerOffCompleteListener;
//import com.acs.bluetooth.BluetoothReader.OnCardStatusAvailableListener;
//import com.acs.bluetooth.BluetoothReader.OnCardStatusChangeListener;
//import com.acs.bluetooth.BluetoothReader.OnDeviceInfoAvailableListener;
//import com.acs.bluetooth.BluetoothReader.OnEnableNotificationCompleteListener;
//import com.acs.bluetooth.BluetoothReader.OnEscapeResponseAvailableListener;
//import com.acs.bluetooth.BluetoothReader.OnResponseApduAvailableListener;
//import com.acs.bluetooth.BluetoothReaderGattCallback;
//import com.acs.bluetooth.BluetoothReaderGattCallback.OnConnectionStateChangeListener;
//import com.acs.bluetooth.BluetoothReaderManager;
//import com.acs.bluetooth.BluetoothReaderManager.OnReaderDetectionListener;
//
//public class ReaderActivity extends AppCompatActivity implements
//        TxPowerDialogFragment.TxPowerDialogListener {
//
//    public static final String TAG = ReaderActivity.class.getSimpleName();
//    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
//    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
//
//    /* Default master key. */
//    private static final String DEFAULT_3901_MASTER_KEY = "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF";
//    /* Get 8 bytes random number APDU. */
//    private static final String DEFAULT_3901_APDU_COMMAND = "80 84 00 00 08";
//
//    /* Default master key. */
//    private static final String DEFAULT_1255_MASTER_KEY = "ACR1255U-J1 Auth";
//
//    /* Read 16 bytes from the binary block 0x04 (MIFARE 1K or 4K). */
//    private static final String DEFAULT_1255_APDU_COMMAND = "FF CA 00 00 00";
//
//    private static final byte[] AUTO_POLLING_START = { (byte) 0xE0, 0x00, 0x00,
//            0x40, 0x01 };
//
//    private static final String DEFAULT_1255_Badge_COMMAND = "FF 82 00 00 06 FF FF FF FF FF FF";
//
//    /* String keys for save/restore instance state. */
//    private static final String AUTHENTICATION_KEY = "authenticatinKey";
//    private static final String APDU_COMMAND = "apduCommand";
//    private static final String RESPONSE_APDU = "responseApdu";
//
//
//    /* Reader to be connected. */
//    private String mDeviceName;
//    private String mDeviceAddress;
//    private int mConnectState = BluetoothReader.STATE_DISCONNECTED;
//
//    /* UI control */
//
//    private Button mAuthentication;
//    private Button mStartPolling;
//
//    private Button mTransmitApdu;
//    private Button badge1;
//    private Button badge2;
//    private Button badge3;
//
//    private TextView mTxtAuthentication;
//
//    private TextView mTxtResponseApdu;
//    private  TextView textView;
//
//    private EditText mEditMasterKey;
//    private EditText mEditApdu;
//
//    private TextView cidTextView;
//    private TextView badgeTextView;
//
//    /* Detected reader. */
//    private BluetoothReader mBluetoothReader;
//    /* ACS Bluetooth reader library. */
//    private BluetoothReaderManager mBluetoothReaderManager;
//    private BluetoothReaderGattCallback mGattCallback;
//
//    private ProgressDialog mProgressDialog;
//
//    /* Bluetooth GATT client. */
//    private BluetoothGatt mBluetoothGatt;
//
//    String cidString;
//    String badgeString;
//
//    /*
//     * Listen to Bluetooth bond status change event. And turns on reader's
//     * notifications once the card reader is bonded.
//     */
//    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            BluetoothAdapter bluetoothAdapter = null;
//            BluetoothManager bluetoothManager = null;
//            final String action = intent.getAction();
//
//            if (!(mBluetoothReader instanceof Acr3901us1Reader)) {
//                /* Only ACR3901U-S1 require bonding. */
//                return;
//            }
//
//            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
//                Log.i(TAG, "ACTION_BOND_STATE_CHANGED");
//
//
//                /* Get bond (pairing) state */
//                if (mBluetoothReaderManager == null) {
//                    Log.w(TAG, "Unable to initialize BluetoothReaderManager.");
//                    return;
//                }
//
//                bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
//                if (bluetoothManager == null) {
//                    Log.w(TAG, "Unable to initialize BluetoothManager.");
//                    return;
//                }
//
//                bluetoothAdapter = bluetoothManager.getAdapter();
//                if (bluetoothAdapter == null) {
//                    Log.w(TAG, "Unable to initialize BluetoothAdapter.");
//                    return;
//                }
//
//                final BluetoothDevice device = bluetoothAdapter
//                        .getRemoteDevice(mDeviceAddress);
//
//                if (device == null) {
//                    return;
//                }
//
//                final int bondState = device.getBondState();
//
//                // TODO: remove log message
//                Log.i(TAG, "BroadcastReceiver - getBondState. state = "
//                        + getBondingStatusString(bondState));
//
//                /* Enable notification */
//                if (bondState == BluetoothDevice.BOND_BONDED) {
//                    if (mBluetoothReader != null) {
//                        mBluetoothReader.enableNotification(true);
//                    }
//                }
//
//                /* Progress Dialog */
//                if (bondState == BluetoothDevice.BOND_BONDING) {
//                    mProgressDialog = ProgressDialog.show(context,
//                            "ACR3901U-S1", "Bonding...");
//                } else {
//                    if (mProgressDialog != null) {
//                        mProgressDialog.dismiss();
//                        mProgressDialog = null;
//                    }
//                }
//
//            }
//        }
//    };
//
//    /* Get the Bonding status string. */
//    private String getBondingStatusString(int bondingStatus) {
//        if (bondingStatus == BluetoothDevice.BOND_BONDED) {
//            return "BOND BONDED";
//        } else if (bondingStatus == BluetoothDevice.BOND_NONE) {
//            return "BOND NONE";
//        } else if (bondingStatus == BluetoothDevice.BOND_BONDING) {
//            return "BOND BONDING";
//        }
//        return "BOND UNKNOWN.";
//    }
//
//    /* Clear the Card reader's response and notification fields. */
//    private void clearAllUi() {
//        /* Clear notification fields. */
//        mTxtAuthentication.setText(R.string.noData);
//
//        /* Clear card reader's response fields. */
//        clearResponseUi();
//    }
//
//    /* Clear the Card reader's Response field. */
//    private void clearResponseUi() {
//        mTxtAuthentication.setText(R.string.noData);
//        mTxtResponseApdu.setText(R.string.noData);
//    }
//
//
//    private void findUiViews() {
//        mAuthentication =  findViewById(R.id.button_Authenticate);
//        mStartPolling =  findViewById(R.id.button_StartPolling);
//        mTransmitApdu =  findViewById(R.id.button_TransmitADPU);
//
//
//        mTxtAuthentication =  findViewById(R.id.textView_Authentication);
//        mTxtResponseApdu =  findViewById(R.id.textView_Response);
//
//        mEditMasterKey =  findViewById(R.id.editText_Master_Key);
//        mEditApdu =  findViewById(R.id.editText_ADPU);
//
//        badge1 = findViewById(R.id.badge1);
//        badge2 = findViewById(R.id.badge2);
//        badge3 = findViewById(R.id.badge3);
//        textView = findViewById(R.id.text);
//
//        cidTextView = findViewById(R.id.cidTextView);
//        badgeTextView = findViewById(R.id.badgeTextView);
//    }
//
//    /*
//     * Update listener
//     */
//    private void setListener(BluetoothReader reader) {
//
//
//        /* Wait for authentication completed. */
//        mBluetoothReader
//                .setOnAuthenticationCompleteListener(new OnAuthenticationCompleteListener() {
//
//                    @Override
//                    public void onAuthenticationComplete(
//                            BluetoothReader bluetoothReader, final int errorCode) {
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (errorCode == BluetoothReader.ERROR_SUCCESS) {
//                                    mTxtAuthentication
//                                            .setText("Authentication Success!");
//                                    mAuthentication.setEnabled(false);
//                                } else {
//                                    mTxtAuthentication
//                                            .setText("Authentication Failed!");
//                                }
//                            }
//                        });
//                    }
//
//                });
//
//        /* Wait for response APDU. */
////        mBluetoothReader
////                .setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
////
////                    @Override
////                    public void onResponseApduAvailable(
////                            BluetoothReader bluetoothReader, final byte[] apdu,
////                            final int errorCode) {
////                        runOnUiThread(new Runnable() {
////                            @Override
////                            public void run() {
//////                                Arrays.copyOfRange(apdu , apdu.length-1 , apdu.length-2);
////
////                            //    byte[] apdu2 = Arrays.copyOfRange(apdu, 2, 6);
////
////                                byte[]apdu2 = Arrays.copyOfRange(apdu , 0 , apdu.length-2);
////
////                                long cid = toReversedDec(apdu2);
////                                mTxtResponseApdu.setText(cid +" "+ getResponseString(apdu2, errorCode));
//////                                mTxtResponseApdu.setText(""+apdu);
////
////                                cidTextView.setText(cid+"");
////
////                            }
////                        });
////                    }
////
////                });
//
//
//        mBluetoothReader
//                .setOnEnableNotificationCompleteListener(new OnEnableNotificationCompleteListener() {
//
//                    @Override
//                    public void onEnableNotificationComplete(
//                            BluetoothReader bluetoothReader, final int result) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (result != BluetoothGatt.GATT_SUCCESS) {
//                                    /* Fail */
//                                    Toast.makeText(
//                                            ReaderActivity.this,
//                                            "The device is unable to set notification!",
//                                            Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(ReaderActivity.this,
//                                            "The device is ready to use!",
//                                            Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        });
//                    }
//
//                });
//    }
//
//
//    private String getErrorString(int errorCode) {
//        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
//            return "";
//        } else if (errorCode == BluetoothReader.ERROR_INVALID_CHECKSUM) {
//            return "The checksum is invalid.";
//        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA_LENGTH) {
//            return "The data length is invalid.";
//        } else if (errorCode == BluetoothReader.ERROR_INVALID_COMMAND) {
//            return "The command is invalid.";
//        } else if (errorCode == BluetoothReader.ERROR_UNKNOWN_COMMAND_ID) {
//            return "The command ID is unknown.";
//        } else if (errorCode == BluetoothReader.ERROR_CARD_OPERATION) {
//            return "The card operation failed.";
//        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_REQUIRED) {
//            return "Authentication is required.";
//        } else if (errorCode == BluetoothReader.ERROR_LOW_BATTERY) {
//            return "The battery is low.";
//        } else if (errorCode == BluetoothReader.ERROR_CHARACTERISTIC_NOT_FOUND) {
//            return "Error characteristic is not found.";
//        } else if (errorCode == BluetoothReader.ERROR_WRITE_DATA) {
//            return "Write command to reader is failed.";
//        } else if (errorCode == BluetoothReader.ERROR_TIMEOUT) {
//            return "Timeout.";
//        } else if (errorCode == BluetoothReader.ERROR_AUTHENTICATION_FAILED) {
//            return "Authentication is failed.";
//        } else if (errorCode == BluetoothReader.ERROR_UNDEFINED) {
//            return "Undefined error.";
//        } else if (errorCode == BluetoothReader.ERROR_INVALID_DATA) {
//            return "Received data error.";
//        } else if (errorCode == BluetoothReader.ERROR_COMMAND_FAILED) {
//            return "The command failed.";
//        }
//        return "Unknown error.";
//    }
//
//    /* Get the Response string. */
//    private String getResponseString(byte[] response, int errorCode) {
//        if (errorCode == BluetoothReader.ERROR_SUCCESS) {
//            if (response != null && response.length > 0) {
//                return Utils.toHexString(response);
//            }
//            return "";
//        }
//        return getErrorString(errorCode);
//    }
//
//
//
//    private void setOnClickListener() {
//        /*
//         * Update onClick listener.
//         */
//
//        /* Authentication function, authenticate the connected card reader. */
//        mAuthentication.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (mBluetoothReader == null) {
//                    mTxtAuthentication.setText(R.string.card_reader_not_ready);
//                    return;
//                }
//
//                /* Retrieve master key from edit box. */
//                byte masterKey[] = Utils.getEditTextinHexBytes(mEditMasterKey);
//
//                if (masterKey != null && masterKey.length > 0) {
//                    /* Clear response field for the result of authentication. */
//                    mTxtAuthentication.setText(R.string.noData);
//
//                    /* Start authentication. */
//                    if (!mBluetoothReader.authenticate(masterKey)) {
//                        mTxtAuthentication
//                                .setText(R.string.card_reader_not_ready);
//                    } else {
//                        mTxtAuthentication.setText("Authenticating...");
//                    }
//                } else {
//                    mTxtAuthentication.setText("Character format error!");
//                }
//            }
//        });
//
//        /* Start polling card. */
//        mStartPolling.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (mBluetoothReader == null) {
//                    Toast.makeText(ReaderActivity.this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                if (!mBluetoothReader.transmitEscapeCommand(AUTO_POLLING_START)) {
//                    Toast.makeText(ReaderActivity.this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        /* Transmit ADPU. */
//        mTransmitApdu.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (mBluetoothReader == null) {
//                    mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                    return;
//                }
//                byte[] apduCommand = Utils.getEditTextinHexBytes(mEditApdu);
//                if (apduCommand != null && apduCommand.length > 0) {
//                    /* Clear response field for result of APDU. */
//                    mTxtResponseApdu.setText(R.string.noData);
//                    /* Transmit APDU command. */
//                    if (!mBluetoothReader.transmitApdu(apduCommand)) {
//                        mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                    } else {
//                        mBluetoothReader.setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
//                            @Override
//                            public void onResponseApduAvailable(BluetoothReader bluetoothReader, final byte[] apdu, final int errorCode) {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        byte[] apdu2 = Arrays.copyOfRange(apdu, 0, apdu.length - 2);
//                                        final long cid = toReversedDec(apdu2);
//                                        if (!getResponseString(apdu2, errorCode).isEmpty()) {
//                                            mTxtResponseApdu.setText(cid + " " + getResponseString(apdu2, errorCode));
//
//                                            cidString = cid + "";
//                                            cidTextView.setText(cid + "");
//                                            //timer.cancel();
//
//                                            new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    byte[] apduCommand1 = new byte[]{(byte) 0xFF, (byte) 0x82, (byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
//                                                    if (!mBluetoothReader.transmitApdu(apduCommand1)) {
//                                                        mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                                                    }
//                                                }
//                                            }, 500);
//
//
//                                            new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    byte[] apduCommand2 = new byte[]{(byte) 0xFF, (byte) 0x86, (byte) 0x00, (byte) 0x00, (byte) 0x05, (byte) 0x01, (byte) 0x00, (byte) 0x04, (byte) 0x60, (byte) 0x00};
//                                                    if (!mBluetoothReader.transmitApdu(apduCommand2)) {
//                                                        mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                                                    }
//                                                }
//                                            }, 1000);
//
//                                            new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    byte[] apduCommand3 = new byte[]{(byte) 0xFF, (byte) 0xB0, (byte) 0x00, (byte) 0x04, (byte) 0x10};
//                                                    if (!mBluetoothReader.transmitApdu(apduCommand3)) {
//                                                        mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                                                    } else {
//                                                        mBluetoothReader.setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
//                                                            @Override
//                                                            public void onResponseApduAvailable(BluetoothReader bluetoothReader, final byte[] apdu, final int errorCode) {
//                                                                runOnUiThread(new Runnable() {
//                                                                    @Override
//                                                                    public void run() {
//                                                                        byte[] apdu2 = Arrays.copyOfRange(apdu, 2, 6);
//                                                                        Log.v("BADGE", apdu2 + "");
//                                                                        Log.v("BADGE2", apdu + "");
//                                                                        long badge = toReversedDec(apdu2);
//                                                                        badgeString = badge + "";
//                                                                        badgeTextView.setText(badge + "");
//
//                                                                        new Handler().postDelayed(new Runnable() {
//                                                                            @Override
//                                                                            public void run() {
//                                                                                cidTextView.setText("");
//                                                                                badgeTextView.setText("");
//                                                                            }
//                                                                        },2000);
//
//                                                                    }
//                                                                });
//                                                            }
//                                                        });
//                                                    }
//                                                }
//                                            }, 1500);
//                                        }
//                                    }
//                                });
//                            }
//                        });
//                    }
//                } else {
//                    mTxtResponseApdu.setText("Character format error!");
//                }
//            }
//        });
//
//        badge1.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                /* Check for detected reader. */
//                if (mBluetoothReader == null) {
//                    mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                    return;
//                }
//
//                /* Retrieve APDU command from edit box. */
//                byte apduCommand[] = new byte[] {(byte)0xFF, (byte)0x82, (byte)0x00, (byte)0x00, (byte)0x06, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,(byte) 0xFF, (byte)0xFF};
//
//
//                if (apduCommand != null && apduCommand.length > 0) {
//                    /* Clear response field for result of APDU. */
//                    mTxtResponseApdu.setText(R.string.noData);
//
//                    /* Transmit APDU command. */
//                    if (!mBluetoothReader.transmitApdu(apduCommand)) {
//                        mTxtResponseApdu
//                                .setText(R.string.card_reader_not_ready);
//                    }
//                    else {
//                        Toast.makeText(ReaderActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    mTxtResponseApdu.setText("Character format error!");
//                }
//            }
//        });
//
//        badge2.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mBluetoothReader == null) {
//                    mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                    return;
//                }
//
//                /* Retrieve APDU command from edit box. */
////                byte apduCommand[] = Utils.getEditTextinHexBytes(mEditApdu);
//
//
//                byte apduCommand[] = new byte[] {(byte)0xFF, (byte)0x86, (byte)0x00, (byte)0x00, (byte)0x05, (byte)0x01, (byte)0x00, (byte)0x04, (byte)0x60, (byte)0x00};
//
////                byte apduCommand[] = new byte[] {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00};
//
//                if (apduCommand != null && apduCommand.length > 0) {
//                    /* Clear response field for result of APDU. */
//                    mTxtResponseApdu.setText(R.string.noData);
//
//                    /* Transmit APDU command. */
//                    if (!mBluetoothReader.transmitApdu(apduCommand)) {
//                        mTxtResponseApdu
//                                .setText(R.string.card_reader_not_ready);
//                    }
//                    else {
//                        Toast.makeText(ReaderActivity.this, "Success 2 ", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    mTxtResponseApdu.setText("Character format error!");
//                }
//            }
//        });
//
//        badge3.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mBluetoothReader == null) {
//                    mTxtResponseApdu.setText(R.string.card_reader_not_ready);
//                    return;
//                }
//
//                /* Retrieve APDU command from edit box. */
//
////                byte apduCommand[] = new byte[] {(byte)0xFF, (byte)0xB0, (byte)0x00, (byte)0x04, (byte)0x10};
//
//                byte apduCommand[] = new byte[] {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00};
//
//                if (apduCommand != null && apduCommand.length > 0) {
//                    /* Clear response field for result of APDU. */
//                    mTxtResponseApdu.setText(R.string.noData);
//
//                    /* Transmit APDU command. */
//                    if (!mBluetoothReader.transmitApdu(apduCommand)) {
//                        mTxtResponseApdu
//                                .setText(R.string.card_reader_not_ready);
//                    }
//
//                    else{
//                        mBluetoothReader
//                                .setOnResponseApduAvailableListener(new OnResponseApduAvailableListener() {
//
//                                    @Override
//                                    public void onResponseApduAvailable(
//                                            BluetoothReader bluetoothReader, final byte[] apdu,
//                                            final int errorCode) {
//                                        runOnUiThread(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                byte[] apdu2 = Arrays.copyOfRange(apdu, 2, 6);
//                                                long badge = toReversedDec(apdu2);
//                                                badgeTextView.setText(badge+"");
//
//                                            }
//                                        });
//                                    }
//
//                                });
//                    }
//                } else {
//                    mTxtResponseApdu.setText("Character format error!");
//                }
//            }
//        });
//
//    }
//
//    /* Start the process to enable the reader's notifications. */
//    private void activateReader(BluetoothReader reader) {
//        if (reader == null) {
//            return;
//        }
//
//        if (reader instanceof Acr3901us1Reader) {
//            /* Start pairing to the reader. */
//            ((Acr3901us1Reader) mBluetoothReader).startBonding();
//        } else if (mBluetoothReader instanceof Acr1255uj1Reader) {
//            /* Enable notification. */
//            mBluetoothReader.enableNotification(true);
//        }
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.reader);
//
//        final Intent intent = getIntent();
//        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
//        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
//
//        /* Update UI. */
//        findUiViews();
//        updateUi(null);
//
//        /* Set the onClick() event handlers. */
//        setOnClickListener();
//
//        /* Initialize BluetoothReaderGattCallback. */
//        mGattCallback = new BluetoothReaderGattCallback();
//
//        /* Register BluetoothReaderGattCallback's listeners */
//        mGattCallback
//                .setOnConnectionStateChangeListener(new OnConnectionStateChangeListener() {
//
//                    @Override
//                    public void onConnectionStateChange(
//                            final BluetoothGatt gatt, final int state,
//                            final int newState) {
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (state != BluetoothGatt.GATT_SUCCESS) {
//                                    /*
//                                     * Show the message on fail to
//                                     * connect/disconnect.
//                                     */
//                                    mConnectState = BluetoothReader.STATE_DISCONNECTED;
//
//                                    if (newState == BluetoothReader.STATE_CONNECTED) {
//
//                                        Toast.makeText(ReaderActivity.this, getString(R.string.connect_fail), Toast.LENGTH_SHORT).show();
//                                    } else if (newState == BluetoothReader.STATE_DISCONNECTED) {
//
//                                        Toast.makeText(ReaderActivity.this, getString(R.string.disconnect_fail), Toast.LENGTH_SHORT).show();
//
//                                    }
//                                    clearAllUi();
//                                    updateUi(null);
//                                    invalidateOptionsMenu();
//                                    return;
//                                }
//
//                                updateConnectionState(newState);
//
//                                if (newState == BluetoothProfile.STATE_CONNECTED) {
//                                    /* Detect the connected reader. */
//                                    if (mBluetoothReaderManager != null) {
//                                        mBluetoothReaderManager.detectReader(
//                                                gatt, mGattCallback);
//                                    }
//                                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//                                    mBluetoothReader = null;
//                                    /*
//                                     * Release resources occupied by Bluetooth
//                                     * GATT client.
//                                     */
//                                    if (mBluetoothGatt != null) {
//                                        mBluetoothGatt.close();
//                                        mBluetoothGatt = null;
//                                    }
//                                }
//                            }
//                        });
//                    }
//                });
//
//        /* Initialize mBluetoothReaderManager. */
//        mBluetoothReaderManager = new BluetoothReaderManager();
//
//        /* Register BluetoothReaderManager's listeners */
//        mBluetoothReaderManager
//                .setOnReaderDetectionListener(new OnReaderDetectionListener() {
//
//                    @Override
//                    public void onReaderDetection(BluetoothReader reader) {
//                        updateUi(reader);
//
//                        if (reader instanceof Acr3901us1Reader) {
//                            /* The connected reader is ACR3901U-S1 reader. */
//                            Log.v(TAG, "On Acr3901us1Reader Detected.");
//                        } else if (reader instanceof Acr1255uj1Reader) {
//                            /* The connected reader is ACR1255U-J1 reader. */
//                            Log.v(TAG, "On Acr1255uj1Reader Detected.");
//                        } else {
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    Toast.makeText(ReaderActivity.this,
//                                            "The device is not supported!",
//                                            Toast.LENGTH_SHORT).show();
//
//                                    /* Disconnect Bluetooth reader */
//                                    Log.v(TAG, "Disconnect reader!!!");
//                                    disconnectReader();
//                                    updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
//                                }
//                            });
//                            return;
//                        }
//
//                        mBluetoothReader = reader;
//                        setListener(reader);
//                        activateReader(reader);
//                    }
//                });
//
//        /* Connect the reader. */
//        connectReader();
//
//        getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getSupportActionBar().setTitle(mDeviceName);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//    }
//
//
//    private boolean connectReader() {
//        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
//        if (bluetoothManager == null) {
//            Log.w(TAG, "Unable to initialize BluetoothManager.");
//            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
//            return false;
//        }
//
//        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
//        if (bluetoothAdapter == null) {
//            Log.w(TAG, "Unable to obtain a BluetoothAdapter.");
//            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
//            return false;
//        }
//
//        /*
//         * Connect Device.
//         */
//        /* Clear old GATT connection. */
//        if (mBluetoothGatt != null) {
//            Log.i(TAG, "Clear old GATT connection");
//            mBluetoothGatt.disconnect();
//            mBluetoothGatt.close();
//            mBluetoothGatt = null;
//        }
//
//        /* Create a new connection. */
//        final BluetoothDevice device = bluetoothAdapter
//                .getRemoteDevice(mDeviceAddress);
//
//        if (device == null) {
//            Log.w(TAG, "Device not found. Unable to connect.");
//            return false;
//        }
//
//        /* Connect to GATT server. */
//        updateConnectionState(BluetoothReader.STATE_CONNECTING);
//        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
//        return true;
//    }
//
//    @Override
//    protected void onResume() {
//        Log.i(TAG, "onResume()");
//        super.onResume();
//
//        final IntentFilter intentFilter = new IntentFilter();
//
//        /* Start to monitor bond state change */
//        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
//        registerReceiver(mBroadcastReceiver, intentFilter);
//
//        /* Clear unused dialog. */
//        if (mProgressDialog != null) {
//            mProgressDialog.dismiss();
//            mProgressDialog = null;
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        Log.i(TAG, "onPause()");
//        super.onPause();
//
//        /* Stop to monitor bond state change */
//        unregisterReceiver(mBroadcastReceiver);
//
//        /* Disconnect Bluetooth reader */
//        disconnectReader();
//    }
//
//    @Override
//    protected void onStop() {
//        Log.i(TAG, "onStop()");
//        super.onStop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        Log.i(TAG, "onDestroy()");
//        super.onDestroy();
//    }
//
//    /* Disconnects an established connection. */
//    private void disconnectReader() {
//        if (mBluetoothGatt == null) {
//            updateConnectionState(BluetoothReader.STATE_DISCONNECTED);
//            return;
//        }
//        updateConnectionState(BluetoothReader.STATE_DISCONNECTING);
//        mBluetoothGatt.disconnect();
//    }
//
//    /* Update the display of Connection status string. */
//    private void updateConnectionState(final int connectState) {
//
//        mConnectState = connectState;
//
//        if (connectState == BluetoothReader.STATE_CONNECTING) {
//            Toast.makeText(this, getString(R.string.connecting), Toast.LENGTH_SHORT).show();
//        } else if (connectState == BluetoothReader.STATE_CONNECTED) {
//            Toast.makeText(this, getString(R.string.connected), Toast.LENGTH_SHORT).show();
//        } else if (connectState == BluetoothReader.STATE_DISCONNECTING) {
//            Toast.makeText(this, getString(R.string.disconnecting), Toast.LENGTH_SHORT).show();
//
//        } else {
//            Toast.makeText(this, getString(R.string.disconnected), Toast.LENGTH_SHORT).show();
//
//            clearAllUi();
//            updateUi(null);
//        }
//        invalidateOptionsMenu();
//    }
//
//    /* Show and hide UI resources and set the default master key and commands. */
//    private void updateUi(final BluetoothReader bluetoothReader) {
//
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (bluetoothReader instanceof Acr3901us1Reader) {
//                    /* The connected reader is ACR3901U-S1 reader. */
//                    if (mEditMasterKey.getText().length() == 0) {
//                        mEditMasterKey.setText(DEFAULT_3901_MASTER_KEY);
//                    }
//                    if (mEditApdu.getText().length() == 0) {
//                        mEditApdu.setText(DEFAULT_3901_APDU_COMMAND);
//                    }
//                    mAuthentication.setEnabled(true);
//                    mStartPolling.setEnabled(false);
//                    mTransmitApdu.setEnabled(true);
//                    mEditMasterKey.setEnabled(true);
//                    mEditApdu.setEnabled(true);
//                } else if (bluetoothReader instanceof Acr1255uj1Reader) {
//                    /* The connected reader is ACR1255U-J1 reader. */
//                    if (mEditMasterKey.getText().length() == 0) {
//                        try {
//                            mEditMasterKey.setText(Utils
//                                    .toHexString(DEFAULT_1255_MASTER_KEY
//                                            .getBytes("UTF-8")));
//                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    if (mEditApdu.getText().length() == 0) {
//                        mEditApdu.setText(DEFAULT_1255_APDU_COMMAND);
//                    }
//
//                    mAuthentication.setEnabled(true);
//                    mStartPolling.setEnabled(true);
//                    mTransmitApdu.setEnabled(true);
//                    mEditMasterKey.setEnabled(true);
//                    mEditApdu.setEnabled(true);
//                } else {
//                    mEditApdu.setText(R.string.noData);
//                    mAuthentication.setEnabled(false);
//                    mStartPolling.setEnabled(false);
//                    mTransmitApdu.setEnabled(false);
//                    mEditMasterKey.setEnabled(false);
//                    mEditApdu.setEnabled(false);
//                }
//            }
//        });
//    }
//
//
//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        /* Save the current state. */
//
//
//        savedInstanceState.putString(AUTHENTICATION_KEY, mEditMasterKey
//                .getText().toString());
//        savedInstanceState.putString(APDU_COMMAND, mEditApdu.getText()
//                .toString());
//        savedInstanceState.putString(RESPONSE_APDU, mTxtResponseApdu.getText()
//                .toString());
//
//        /* Always call the superclass so it can save the view hierarchy state. */
//        super.onSaveInstanceState(savedInstanceState);
//    }
//
//    @Override
//    public void onRestoreInstanceState(Bundle savedInstanceState) {
//        /* Always call the superclass so it can restore the view hierarchy. */
//        super.onRestoreInstanceState(savedInstanceState);
//
//        /* Restore state members from saved instance. */
//
//
//        mEditMasterKey
//                .setText(savedInstanceState.getString(AUTHENTICATION_KEY));
//        mEditApdu.setText(savedInstanceState.getString(APDU_COMMAND));
//        mTxtResponseApdu.setText(savedInstanceState.getString(RESPONSE_APDU));
//    }
//
//
//    @Override
//    public void onDialogItemClick(DialogFragment dialog, int which) {
//        byte[] command = { (byte) 0xE0, 0x00, 0x00, 0x49, (byte) which };
//
//        if (mBluetoothReader == null) {
//
//            Toast.makeText(this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//        if (!mBluetoothReader.transmitEscapeCommand(command)) {
//            Toast.makeText(this, getString(R.string.card_reader_not_ready), Toast.LENGTH_SHORT).show();        }
//    }
//
//    private long toReversedDec(byte[] bytes) {
//        long result = 0;
//        long factor = 1;
//        for (int i = bytes.length - 1; i >= 0; --i) {
//            long value = bytes[i] & 0xffl;
//            result += value * factor;
//            factor *= 256l;
//        }
//        return result;
//    }
//
//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//
//    }
//}
