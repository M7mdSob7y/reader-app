package com.acs.btdemo;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.multidex.MultiDex;

import com.acs.btdemo.helper.JSONObjectBuilder;
import com.acs.btdemo.helper.StringPOSTRequest;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class LoginActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener{

    private EditText userNameEditText , passwordEditText;
    private Button signInButton;

    private Key privateKey;
    //private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        if (prefs.getBoolean("LOGGED_IN", false)) {
//            Intent intent = new Intent(this, DeviceScanActivity.class);
//            startActivity(intent);
//        }

        userNameEditText = findViewById(R.id.signIn_userName_editText);
        passwordEditText = findViewById(R.id.signIn_password_editText);
        //textView = findViewById(R.id.textView6);
        signInButton = findViewById(R.id.signIn_sign_in_button);

        //getting android ID
        String androidId = Settings.System.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //making private and public key
        Key publicKey = null;
        privateKey = null;
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            publicKey = kp.getPublic();
            privateKey = kp.getPrivate();
        } catch (Exception e) {
            Toast.makeText(this, "no key", Toast.LENGTH_LONG).show();
        }

        //sending key to backend
        if (!prefs.contains("KEY")) {
            JSONObjectBuilder JSONObjectBuilder = null;
            try {
                JSONObjectBuilder = new JSONObjectBuilder(getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject json = JSONObjectBuilder.buildRSAKeyJSONObject(androidId, publicKey.toString().substring(28, 284));

            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            String requestURL = getResources().getString(R.string.url);

            String jsons = json.toString();


            StringPOSTRequest request = new StringPOSTRequest(requestURL, jsons,
                     LoginActivity.this, LoginActivity.this);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag(0);
            queue.add(request);
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                next();
            }
        });
    }

    public void next() {
        // Do something in response to button
        if (userNameEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please Enter USERNAME", Toast.LENGTH_SHORT).show();
            userNameEditText.requestFocus();
        } else if (passwordEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please Enter PASSWORD", Toast.LENGTH_SHORT).show();
            passwordEditText.requestFocus();
        } else {
            String username = userNameEditText.getText().toString(), pass = passwordEditText.getText().toString();
            //textView = (TextView) findViewById(R.id.textView6);

            JSONObjectBuilder JSONObjectBuilder = null;

            try {
                JSONObjectBuilder = new JSONObjectBuilder(getApplicationContext());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject json = JSONObjectBuilder.buildUserJSONObject(username, pass);


            RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
            String requestURL = getResources().getString(R.string.url);

            String jsons = json.toString();


            StringPOSTRequest request = new StringPOSTRequest(requestURL, jsons,
                     LoginActivity.this,  LoginActivity.this);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            request.setTag(0);
            queue.add(request);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
//        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
//        textView.append("Transaction Failed " + error.toString());
//        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();

        new AlertDialog.Builder(this).setTitle("Error").setMessage(error.toString())
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }

    @Override
    public void onResponse(String response) {
        Log.w("myApp", response);
        String TXN_STATUS = "";
        Boolean CAN_TOPUP = null;
        String MERCHANT_NAME = "", AGENT_ID = "", NO_OF_AGENT = "", USERNAME = "", BALANCE = "", TYPE = "", KEY2 = "";
        try {
            JSONObject obj = new JSONObject(response);
            TYPE = obj.getString("TYPE");
            TXN_STATUS = obj.getString("TXNSTATUS");
            CAN_TOPUP = obj.getBoolean("CAN_TOPUP");
            MERCHANT_NAME = obj.getString("MERCHANT_NAME");
            AGENT_ID = obj.getString("AGENT_ID");
            BALANCE = obj.getString("BALANCE");
            KEY2 = obj.getString("KEY2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject obj = new JSONObject(response);
            TYPE = obj.getString("TYPE");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (TYPE.equals("MPOSINQRES")) {
            switch (Integer.valueOf(TXN_STATUS)) {
                case 200:
                    SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    timeFormat.setTimeZone(TimeZone.getTimeZone("Africa/Cairo"));
                    String time = timeFormat.format(new Date());

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("CAN_TOPUP", CAN_TOPUP);
                    editor.putString("MERCHANT_NAME", MERCHANT_NAME);
                    editor.putString("AGENT_ID", AGENT_ID);
                    editor.putString("BALANCE", BALANCE);
                    editor.putBoolean("LOGGED_IN", true);

                    if (!prefs.contains("DATE")) {
                        editor.putString("DATE", time);
                    }
                    editor.apply();

                    Intent intent = new Intent(this, DeviceScanActivity.class);
                    startActivity(intent);
                    break;
                default:
                    Toast.makeText(this, "Wrong Username Or password", Toast.LENGTH_LONG).show();
            }
        } else if (TYPE.equals("MPOSKEYRES")) {
            //store the private key
            byte[] privKeyBytes = privateKey.getEncoded();
            String privKeyStr = Base64.encodeToString(privKeyBytes, Base64.DEFAULT);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            editor.remove("KEY");
            editor.putString("KEY", privKeyStr);
            editor.apply();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean("LOGGED_IN", false) == true && prefs.contains("LOGGED_IN")) {
            Intent intent = new Intent(this, DeviceScanActivity.class);
            startActivity(intent);
        }
        userNameEditText.setText("");
        passwordEditText.setText("");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_CALL:
                next();
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}