package com.acs.btdemo.helper;

import android.view.View;
import android.widget.EditText;

/**
 * Created by MTawfik on Aug/30/17.
 */

public class MoneyClickListener implements View.OnClickListener {
    EditText editText;

    public MoneyClickListener(EditText e) {
        editText = e;
    }
    @Override
    public void onClick(View v) {
        editText.setSelection(editText.getText().length());
    }
}
