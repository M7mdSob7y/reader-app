package com.acs.btdemo.helper;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.toolbox.StringRequest;


/**
 * Created by Mostafa on 3/7/2016.
 */
public class StringPOSTRequest extends StringRequest {

    private String postContents;

    public StringPOSTRequest(String url, String postContents, Response.Listener<String> listener, ErrorListener errorListener) {
        super(Request.Method.POST, url, listener, errorListener);
        this.setShouldCache(false);
        this.postContents = postContents;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return postContents.getBytes();
    }

    @Override
    public String getBodyContentType()
    {
        return "application/json";
    }

}