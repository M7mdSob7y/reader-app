package com.acs.btdemo;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.acs.btdemo.helper.JSONObjectBuilder;
import com.acs.btdemo.helper.StringPOSTRequest;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class BalanceActivity extends AppCompatActivity {

    private TextView cid;
    private TextView badge;

    // Define Variable to access Token
    private String username = "30001";
    private String password = "123456";
    private String grant_type = "password";
    private String client_id = "mfNEXriHyUN5AHrSVb6QdOmxVmF2s0KNiwaOD2rH";
    private String client_secret = "c1cSDSZ9y1KHDcBg9aSie46YDfpetttovZnXXEQL0su0oTNbDlCKofD3SBu14EnGqTM0WfKXifnGTvZc577tHokFwglj2cxez1nxztSd6E1ztnuKM9nidhru7pFfzkVL";
    private String requestData;
    public OkHttpClient client;
    public static final MediaType CONTENT_TYPE = MediaType.get("application/x-www-form-urlencoded");
    private String apUrl = "http://stgg40.paymobsolutions.com/api-auth-token/token/";
    private String access;
    private String access_token;

    //Define Variable to Balance Response
    private String firstName;
    private String lasrName;
    private String balance;
    private String badgeString;

    private TextView auth_token_textView;
    private TextView hmac_textView;
    private TextView firstNAme_textView;
    private TextView lastNAme_textView;
    private TextView balance_textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        cid = findViewById(R.id.textView_cid);
        badge = findViewById(R.id.textView_badge);

        findUiViews();

        badgeString = getIntent().getStringExtra("BADGE");
        cid.setText(getIntent().getStringExtra("CID"));
        badge.setText(badgeString);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(BalanceActivity.this , ReaderActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);

//        client = new OkHttpClient.Builder()
//                //.addInterceptor(new BasicAuthInterceptor("mfNEXriHyUN5AHrSVb6QdOmxVmF2s0KNiwaOD2rH", "c1cSDSZ9y1KHDcBg9aSie46YDfpetttovZnXXEQL0su0oTNbDlCKofD3SBu14EnGqTM0WfKXifnGTvZc577tHokFwglj2cxez1nxztSd6E1ztnuKM9nidhru7pFfzkVL"))
//                .build();
//
//        requestData = "grant_type=" + grant_type + "&username=" + username + "&password=" + password + "&client_id=" + client_id + "&client_secret=" + client_secret;
//
//        try {
//            loginRequest(apUrl, requestData);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        hmac_textView.setText(hmacString());
    }

    //FindViewByID
    private void findUiViews() {
        auth_token_textView = findViewById(R.id.auth_token_textView);
        hmac_textView = findViewById(R.id.hmac_textView);
        firstNAme_textView = findViewById(R.id.firstNAme_textView);
        lastNAme_textView = findViewById(R.id.lastNAme_textView);
        balance_textView = findViewById(R.id.balance_textView);
    }

    //Request Access Token
//    public void loginRequest(String apUrl, String requestData) throws IOException {
//
//        RequestBody body = RequestBody.create(CONTENT_TYPE, requestData);
//        Request request = new Request.Builder()
//                .url(apUrl)
//                .addHeader("Content-Type", " application/x-www-form-urlencoded")
//                .post(body)
//                .build();
//
//        Call call = client.newCall(request);
//        call.enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.e("CallBackResponse", "onFailure() Request was: " + call.request());
//                e.printStackTrace();
//            }
//
//            @Override
//            public void onResponse(Call call, okhttp3.Response response) throws IOException {
////                 Log.v("CallBackResponse ", "onResponse(): " + response.body().string());
//                access = response.body().string();
//
//                try {
//
//                    JSONObject jsonObject = new JSONObject(access);
//                    access_token = jsonObject.getString("access_token");
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                BalanceActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        auth_token_textView.setText(access_token);
//                        JSONObjectBuilder JSONObjectBuilder = null;
//                        try {
//                            JSONObjectBuilder = new JSONObjectBuilder(getApplicationContext());
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        JSONObject json = JSONObjectBuilder.buildBalanceInquiryJSONObject("30001", "123456", badgeString);
//
//                        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
//
//                        String requestURL = "http://stgg40.paymobsolutions.com/NewTxn/ServiceSelector/v2/?hmac=" + hmacString();
//
//                        String jsons = json.toString();
//
//                        StringPOSTRequest request = new StringPOSTRequest(requestURL, jsons,
//                                BalanceActivity.this, BalanceActivity.this) {
//                            @Override
//                            public Map<String, String> getHeaders() throws AuthFailureError {
//                                HashMap<String, String> param = new HashMap<String, String>();
//                                param.put("Authorization", "Bearer "+access_token);
//                                return param;
//                            }
//                        };
//                        request.setRetryPolicy(new DefaultRetryPolicy(
//                                20000,
//                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//                        request.setTag(0);
//                        queue.add(request);
//                    }
//                });
//
//            }
//
//        });
//
//    }

    //get hmac method
//    private String hmacString() {
//
//        String key = "519b9a18eddfd151afae0293186f3088851cabb5e7757cde2567a0488bc29116";
//        String message = "40012102030"+badgeString;
//
//        Mac hasher = null;
//        try {
//            hasher = Mac.getInstance("HmacSHA512");
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        try {
//            hasher.init(new SecretKeySpec(key.getBytes(), "HmacSHA512"));
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        }
//
//        byte[] hash = hasher.doFinal(message.getBytes());
//
//        return toReversedHex(hash);
//    }

    //Reverse Byte Array to Hex String
//    private String toReversedHex(byte[] bytes) {
//        StringBuilder sb = new StringBuilder();
//        for (int i = 0; i < bytes.length; ++i) {
//            int b = bytes[i] & 0xff;
//            if (b < 0x10)
//                sb.append('0');
//            sb.append(Integer.toHexString(b));
//        }
//        return sb.toString();
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }
//
//    @Override
//    public void onErrorResponse(VolleyError error) {
//        Log.v("ERROR_FROM_THE_SERVER" , error.toString());
//    }
//
//    @Override
//    public void onResponse(String response) {
//        try {
//            JSONObject jsonObject = new JSONObject(response);
//
//            firstName = jsonObject.getString("FIRSTNAME");
//            lasrName = jsonObject.getString("LASTNAME");
//            balance = jsonObject.getString("BALANCE");
//
//            firstNAme_textView.setText(firstName);
//            lastNAme_textView.setText(lasrName);
//            balance_textView.setText(balance);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
}
